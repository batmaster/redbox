<!-- Username Field -->
<div class="form-group col-sm-6">
    {!! Form::label('username', 'Username:') !!}
    {!! Form::text('username', null, ['class' => 'form-control']) !!}
</div>

<!-- Acctstarttime Field -->
<div class="form-group col-sm-6">
    {!! Form::label('acctstarttime', 'Acctstarttime:') !!}
    {!! Form::text('acctstarttime', null, ['class' => 'form-control']) !!}
</div>

<!-- Acctstoptime Field -->
<div class="form-group col-sm-6">
    {!! Form::label('acctstoptime', 'Acctstoptime:') !!}
    {!! Form::text('acctstoptime', null, ['class' => 'form-control']) !!}
</div>

<!-- Calledstationid Field -->
<div class="form-group col-sm-6">
    {!! Form::label('calledstationid', 'Calledstationid:') !!}
    {!! Form::text('calledstationid', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('radAccts.index') !!}" class="btn btn-default">Cancel</a>
</div>
