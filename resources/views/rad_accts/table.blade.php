<table class="table table-responsive" id="radAccts-table">
    <thead>
        <tr>
            <th>ID</th>
            <th>Username</th>
            <th>Acctstarttime</th>
            <th>Acctstoptime</th>
            <th>Calledstationid</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($radAccts as $radAcct)
        <tr>
            <td>{!! $radAcct->radacctid !!}</td>
            <td>{!! $radAcct->username !!}</td>
            <td>{!! $radAcct->acctstarttime !!}</td>
            <td>{!! $radAcct->acctstoptime !!}</td>
            <td>{!! $radAcct->calledstationid !!}</td>
            <td>
                {!! Form::open(['route' => ['radAccts.destroy', $radAcct->radacctid], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('radAccts.show', [$radAcct->radacctid]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('radAccts.edit', [$radAcct->radacctid]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
