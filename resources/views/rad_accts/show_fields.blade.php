<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $radAcct->id !!}</p>
</div>

<!-- Username Field -->
<div class="form-group">
    {!! Form::label('username', 'Username:') !!}
    <p>{!! $radAcct->username !!}</p>
</div>

<!-- Acctstarttime Field -->
<div class="form-group">
    {!! Form::label('acctstarttime', 'Acctstarttime:') !!}
    <p>{!! $radAcct->acctstarttime !!}</p>
</div>

<!-- Acctstoptime Field -->
<div class="form-group">
    {!! Form::label('acctstoptime', 'Acctstoptime:') !!}
    <p>{!! $radAcct->acctstoptime !!}</p>
</div>

<!-- Calledstationid Field -->
<div class="form-group">
    {!! Form::label('calledstationid', 'Calledstationid:') !!}
    <p>{!! $radAcct->calledstationid !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $radAcct->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $radAcct->updated_at !!}</p>
</div>

