<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class RadAcct
 * @package App\Models
 * @version May 2, 2018, 1:59 pm UTC
 *
 * @property string username
 * @property string acctstarttime
 * @property string acctstoptime
 * @property string calledstationid
 */
class RadAcct extends Model
{
    use SoftDeletes;

    public $table = 'radacct';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'username',
        'acctstarttime',
        'acctstoptime',
        'calledstationid'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'username' => 'string',
        'acctstarttime' => 'string',
        'acctstoptime' => 'string',
        'calledstationid' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'username' => 'required'
    ];


}
