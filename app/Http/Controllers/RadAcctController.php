<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateRadAcctRequest;
use App\Http\Requests\UpdateRadAcctRequest;
use App\Repositories\RadAcctRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class RadAcctController extends AppBaseController
{
    /** @var  RadAcctRepository */
    private $radAcctRepository;

    public function __construct(RadAcctRepository $radAcctRepo)
    {
        $this->radAcctRepository = $radAcctRepo;
    }

    /**
     * Display a listing of the RadAcct.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->radAcctRepository->pushCriteria(new RequestCriteria($request));
        $radAccts = $this->radAcctRepository->paginate(25);

        return view('rad_accts.index')
            ->with('radAccts', $radAccts);
    }

    /**
     * Show the form for creating a new RadAcct.
     *
     * @return Response
     */
    public function create()
    {
        return view('rad_accts.create');
    }

    /**
     * Store a newly created RadAcct in storage.
     *
     * @param CreateRadAcctRequest $request
     *
     * @return Response
     */
    public function store(CreateRadAcctRequest $request)
    {
        $input = $request->all();

        $radAcct = $this->radAcctRepository->create($input);

        Flash::success('Rad Acct saved successfully.');

        return redirect(route('radAccts.index'));
    }

    /**
     * Display the specified RadAcct.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $radAcct = $this->radAcctRepository->findWithoutFail($id);

        if (empty($radAcct)) {
            Flash::error('Rad Acct not found');

            return redirect(route('radAccts.index'));
        }

        return view('rad_accts.show')->with('radAcct', $radAcct);
    }

    /**
     * Show the form for editing the specified RadAcct.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $radAcct = $this->radAcctRepository->findWithoutFail($id);

        if (empty($radAcct)) {
            Flash::error('Rad Acct not found');

            return redirect(route('radAccts.index'));
        }

        return view('rad_accts.edit')->with('radAcct', $radAcct);
    }

    /**
     * Update the specified RadAcct in storage.
     *
     * @param  int              $id
     * @param UpdateRadAcctRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRadAcctRequest $request)
    {
        $radAcct = $this->radAcctRepository->findWithoutFail($id);

        if (empty($radAcct)) {
            Flash::error('Rad Acct not found');

            return redirect(route('radAccts.index'));
        }

        $radAcct = $this->radAcctRepository->update($request->all(), $id);

        Flash::success('Rad Acct updated successfully.');

        return redirect(route('radAccts.index'));
    }

    /**
     * Remove the specified RadAcct from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $radAcct = $this->radAcctRepository->findWithoutFail($id);

        if (empty($radAcct)) {
            Flash::error('Rad Acct not found');

            return redirect(route('radAccts.index'));
        }

        $this->radAcctRepository->delete($id);

        Flash::success('Rad Acct deleted successfully.');

        return redirect(route('radAccts.index'));
    }
}
