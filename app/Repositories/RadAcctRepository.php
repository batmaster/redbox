<?php

namespace App\Repositories;

use App\Models\RadAcct;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class RadAcctRepository
 * @package App\Repositories
 * @version May 2, 2018, 1:59 pm UTC
 *
 * @method RadAcct findWithoutFail($id, $columns = ['*'])
 * @method RadAcct find($id, $columns = ['*'])
 * @method RadAcct first($columns = ['*'])
*/
class RadAcctRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'username',
        'acctstarttime',
        'acctstoptime',
        'calledstationid'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return RadAcct::class;
    }
}
